-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema Component_Store
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Component_Store
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Component_Store` DEFAULT CHARACTER SET utf8 ;
USE `Component_Store` ;

-- -----------------------------------------------------
-- Table `Component_Store`.`article`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Component_Store`.`article` ;

CREATE TABLE IF NOT EXISTS `Component_Store`.`article` (
  `idArticle` INT(11) NOT NULL AUTO_INCREMENT,
  `articleName` VARCHAR(30) NOT NULL,
  `quantity` INT(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idArticle`),
  UNIQUE INDEX `idArticle_UNIQUE` (`idArticle` ASC) ,
  UNIQUE INDEX `articleName_UNIQUE` (`articleName` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Component_Store`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Component_Store`.`user` ;

CREATE TABLE IF NOT EXISTS `Component_Store`.`user` (
  `idUser` INT(11) NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(15) NOT NULL,
  `lastName` VARCHAR(15) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(300) NOT NULL,
  `banned` TINYINT(1) NOT NULL,
  `roles` LONGTEXT NOT NULL,
  `ban_date` DATETIME NULL DEFAULT NULL,
  `ban_end_date` DATETIME NULL DEFAULT NULL,
  `securityCode` VARCHAR(30) NOT NULL,
  `activated` TINYINT(1) NOT NULL,
  `evaluation` FLOAT NULL DEFAULT NULL,
  PRIMARY KEY (`idUser`),
  UNIQUE INDEX `iduser_UNIQUE` (`idUser` ASC) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) ,
  UNIQUE INDEX `securityCode_UNIQUE` (`securityCode` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;


-- -----------------------------------------------------
-- Table `Component_Store`.`provider`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Component_Store`.`provider` ;

CREATE TABLE IF NOT EXISTS `Component_Store`.`provider` (
  `idProvider` INT(11) NOT NULL AUTO_INCREMENT,
  `providerName` VARCHAR(30) NOT NULL,
  `reliable` TINYINT(1) NOT NULL,
  `globalGrade` FLOAT NULL DEFAULT NULL,
  `idComment` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idProvider`),
  UNIQUE INDEX `idProvider_UNIQUE` (`idProvider` ASC) ,
  UNIQUE INDEX `providerName_UNIQUE` (`providerName` ASC) ,
  UNIQUE INDEX `idComment_UNIQUE` (`idComment` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Component_Store`.`comment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Component_Store`.`comment` ;

CREATE TABLE IF NOT EXISTS `Component_Store`.`comment` (
  `idComment` INT(11) NOT NULL AUTO_INCREMENT,
  `content` VARCHAR(500) NOT NULL,
  `postDate` DATETIME NOT NULL,
  `idUser` INT(11) NOT NULL,
  `idProvider` INT(11) NOT NULL,
  PRIMARY KEY (`idComment`),
  UNIQUE INDEX `idComment_UNIQUE` (`idComment` ASC) ,
  CONSTRAINT `fk_comment_1`
    FOREIGN KEY (`idUser`)
    REFERENCES `Component_Store`.`user` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_2`
    FOREIGN KEY (`idProvider`)
    REFERENCES `Component_Store`.`provider` (`idProvider`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Component_Store`.`onlineOrder`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Component_Store`.`onlineOrder` ;

CREATE TABLE IF NOT EXISTS `Component_Store`.`onlineOrder` (
  `idonlineOrder` INT(11) NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(500) NOT NULL,
  `inspectionDate` DATETIME NULL DEFAULT NULL,
  `status` VARCHAR(45) NOT NULL,
  `creationDate` DATETIME NOT NULL,
  `idUser` INT(11) NOT NULL,
  `pdfContent` MEDIUMTEXT NULL DEFAULT NULL,
  `urgent` TINYINT(4) NOT NULL DEFAULT 0,
  `personal` TINYINT(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`idonlineOrder`),
  UNIQUE INDEX `idonlineOrder_UNIQUE` (`idonlineOrder` ASC) ,
  INDEX `fk_onlineOrder_1` (`idUser` ASC) ,
  CONSTRAINT `fk_onlineOrder_1`
    FOREIGN KEY (`idUser`)
    REFERENCES `Component_Store`.`user` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Component_Store`.`contains`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Component_Store`.`contains` ;

CREATE TABLE IF NOT EXISTS `Component_Store`.`contains` (
  `idArt` INT(11) NOT NULL,
  `idonlineOrder` INT(11) NOT NULL,
  PRIMARY KEY (`idArt`, `idonlineOrder`),
  UNIQUE INDEX `idOrder_UNIQUE` (`idonlineOrder` ASC) ,
  UNIQUE INDEX `idArt_UNIQUE` (`idArt` ASC) ,
  CONSTRAINT `fk_contains_1`
    FOREIGN KEY (`idArt`)
    REFERENCES `Component_Store`.`article` (`idArticle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contains_2`
    FOREIGN KEY (`idonlineOrder`)
    REFERENCES `Component_Store`.`onlineOrder` (`idonlineOrder`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Component_Store`.`evaluation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Component_Store`.`evaluation` ;

CREATE TABLE IF NOT EXISTS `Component_Store`.`evaluation` (
  `idEvaluation` INT(11) NOT NULL AUTO_INCREMENT,
  `rate` INT(11) NOT NULL,
  `idRater` INT(11) NOT NULL,
  `idRated` INT(11) NOT NULL,
  PRIMARY KEY (`idEvaluation`),
  UNIQUE INDEX `idEvaluation_UNIQUE` (`idEvaluation` ASC) ,
  INDEX `fk_evaluation_1_idx` (`idRater` ASC) ,
  INDEX `fk_evaluation_2_idx` (`idRated` ASC) ,
  CONSTRAINT `fk_evaluation_1`
    FOREIGN KEY (`idRater`)
    REFERENCES `Component_Store`.`user` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_evaluation_2`
    FOREIGN KEY (`idRated`)
    REFERENCES `Component_Store`.`user` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Component_Store`.`grade`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Component_Store`.`grade` ;

CREATE TABLE IF NOT EXISTS `Component_Store`.`grade` (
  `idGrade` INT(11) NOT NULL AUTO_INCREMENT,
  `rate` INT(11) NOT NULL,
  `idUser` INT(11) NOT NULL,
  `idProvider` INT(11) NOT NULL,
  PRIMARY KEY (`idGrade`),
  UNIQUE INDEX `idGrade_UNIQUE` (`idGrade` ASC) ,
  INDEX `fk_grade_1` (`idUser` ASC) ,
  INDEX `fk_grade_2` (`idProvider` ASC) ,
  CONSTRAINT `fk_grade_1`
    FOREIGN KEY (`idUser`)
    REFERENCES `Component_Store`.`user` (`idUser`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_grade_2`
    FOREIGN KEY (`idProvider`)
    REFERENCES `Component_Store`.`provider` (`idProvider`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 32
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Component_Store`.`provides`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Component_Store`.`provides` ;

CREATE TABLE IF NOT EXISTS `Component_Store`.`provides` (
  `idProvider` INT(11) NOT NULL,
  `idArticle` INT(11) NOT NULL,
  PRIMARY KEY (`idProvider`, `idArticle`),
  UNIQUE INDEX `idProvider_UNIQUE` (`idProvider` ASC) ,
  UNIQUE INDEX `idArticle_UNIQUE` (`idArticle` ASC) ,
  CONSTRAINT `fk_provides_1`
    FOREIGN KEY (`idArticle`)
    REFERENCES `Component_Store`.`article` (`idArticle`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_provides_2`
    FOREIGN KEY (`idProvider`)
    REFERENCES `Component_Store`.`provider` (`idProvider`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
