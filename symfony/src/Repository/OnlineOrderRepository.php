<?php

namespace App\Repository;

use App\Entity\Onlineorder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Onlineorder|null find($id, $lockMode = null, $lockVersion = null)
 * @method Onlineorder|null findOneBy(array $criteria, array $orderBy = null)
 * @method Onlineorder[]    findAll()
 * @method Onlineorder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OnlineOrderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Onlineorder::class);
    }

    /**
     * @return Onlineorder[] Returns an array of Onlineorder objects
     */
    public function findByStatus($status)
    {
        return $this->createQueryBuilder('onlineOrder')
            ->andWhere('onlineOrder.status = :status')
            ->setParameter('status', $status)
            ->orderBy('onlineOrder.idonlineorder', 'ASC')
            ->setMaxResults(50)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @return Onlineorder[] Returns an array of Onlineorder objects
     */
    public function  findUrgent($urgent){
        return $this->createQueryBuilder('onlineOrder')
            ->andWhere('onlineOrder.urgent = :value')
            ->setParameter('value', $urgent)
            ->orderBy('onlineOrder.idonlineorder', 'ASC')
            ->setMaxResults(50)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return Onlineorder[] Returns an array of Onlineorder objects
     */
    public function  findPersonal($personal){
        return $this->createQueryBuilder('onlineOrder')
            ->andWhere('onlineOrder.personal = :personal')
            ->setParameter('personal', $personal)
            ->orderBy('onlineOrder.idonlineorder', 'ASC')
            ->setMaxResults(50)
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?Onlineorder
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
