<?php

namespace App\Controller;


//Required for PDF conversion
$parent1 = dirname(__DIR__);
$root = dirname($parent1);
require $root.'/vendor/autoload.php';

use Doctrine\Common\Persistence\ManagerRegistry;
use Spipu\Html2Pdf\Html2Pdf;
use App\Entity\Onlineorder;
use App\Entity\User;
use App\Form\OnlineorderType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 *
 * @Route("/onlineorder")
 */
class OnlineorderController extends SecurityController
{

    ////////////////
    ///  ROUTES  ///
    ////////////////

    /**
     * @Route("/", name="onlineorder_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $status = $request->query->get('onlineOrders');
        $manag = $this->getDoctrine();

        $onlineorders=$this->switchStatus($status, $manag);

        return $this->render('onlineorder/index.html.twig', [
            'title' => "Orders",
            'headtitle' => "Choose the orders you want to see",
            'onlineorders' => $onlineorders,
        ]);
    }

    /**
     * @Route("/{iduser}/new", name="onlineorder_new", methods={"GET","POST"})
     */
    public function new(\Swift_Mailer $mailer, Request $request, User $user): Response
    {
        $this->denyAccssIfNotActivated($user);
        $this->denyAccessIfNotCurrentUser($user);

        $totalPrice=0;
        $totalNumber=0;
        $list=$this->container->get('session')->get('list');
        $onlineorder = new Onlineorder();
        $onlineorder->setIduser($user);

        $roles = $user->getRoles();

        if( $roles[0] == 'ROLE_STUDENT' ){
            $form = $this->createFormBuilder($onlineorder)
                ->add('description')
                ->getForm();

        } else {
            $form = $this->createFormBuilder($onlineorder)
                ->add('description')
                ->add('urgent')
                ->add('personal')
                ->getForm();
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $onlineorder) {
            //Method to create the whole order
            $this->createFromOrder($user, $onlineorder, $list);

            //Sends an email when the order is confirmed
            $this->sendEmail($mailer, 'Order confirm', $user, 'emails/confirmOrder.html.twig', [
                    'order' => $onlineorder,
                    'user' =>  $onlineorder->getIduser(),
                    'message' => $onlineorder->getStatus(),
                    'list' => $list,
                ]
            );

            //Clear the temporary list of items
            $list->clear();
            return $this->redirectToRoute('onlineorder_showAll', array('iduser'=>$user->getIduser()));
        }
        foreach ($list as $item){
            $totalNumber+=$item->getQuantity();
            $totalPrice+=($item->getPrice()*$item->getQuantity());
        }
        return $this->render('onlineorder/new.html.twig', [
            'title' => "New Order",
            'headtitle' => "Create your new order",
            'onlineorder' => $onlineorder,
            'list' => $list,
            'user' => $user,
            'totalPrice' => $totalPrice,
            'totalNumber' => $totalNumber,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idonlineorder}", name="onlineorder_show", methods={"GET"})
     */
    public function show(Onlineorder $onlineorder): Response
    {
        $this->denyAccssIfNotActivated($onlineorder->getIduser());
        $this->denyAccessIfNotCurrentUser($onlineorder->getIduser());

        return $this->render('onlineorder/show.html.twig', [
            'onlineorder' => $onlineorder,
        ]);
    }

    /**
     * Shows the orders of one user
     *
     * @Route("/{iduser}/allOrders", name="onlineorder_showAll", methods={"GET"})
     */
    public function showAll(User $user): Response
    {
        $this->denyAccssIfNotActivated($user);
        $this->denyAccessIfNotCurrentUser($user);
        return $this->render('onlineorder/showAllForClient.html.twig', [
            'headtitle' => "Your orders",
            'user'=> $user,
        ]);
    }


    /**
     * @Route("/{iduser}/allOrders/{idonlineorder}", name="onlineorder_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user, Onlineorder $onlineorder): Response
    {
        $this->denyAccssIfNotActivated($user);

        if( !$this->isSelfUser($user)){
            throw new AccessDeniedException();
        }
        $this->denyAccessIfNotCurrentUser($user);

        $submittedToken = $request->request->get('_token');

        if ($this->isCsrfTokenValid('delete'.$onlineorder->getIdonlineorder(), $submittedToken)) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($onlineorder);
            $entityManager->flush();
        }
        return $this->redirectToRoute('onlineorder_showAll', [
            'iduser' => $user->getIduser(),
        ]);
    }

    /**
     * @Route("/{idonlineorder}/changeStatus/{idButton}", name="onlineorder_changeStatus", methods={"GET","POST"})
     */
    public function changeStatus(\Swift_Mailer $mailer, Request $request, Onlineorder $onlineorder, int $idButton): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $now = new \DateTime('NOW');
        $req = new Request();

        $message = $onlineorder->STATUSES[$idButton];

        $this->switchIdButton($idButton, $onlineorder, $req, $mailer, $message);

        $onlineorder->setInspectiondate($now);
        $this->save($onlineorder);
        return $this->redirectToRoute("onlineorder_index");
    }

    /**
     * @Route("/{idonlineorder}/reason/{idButton}", name="onlineorder_reason", methods={"GET","POST"})
     */
    public function reason(Request $request, \Swift_Mailer $mailer, Onlineorder $onlineorder, int $idButton){
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        if($idButton == 7) $defaultData = ['message' => 'Declined'];
        else $defaultData = ['message' => 'Canceled'];

        $form = $this->createFormBuilder($defaultData)
            ->add('message', TextareaType::class)
            ->add('send', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $onlineorder->getIduser();
            $subject = ($idButton == 7) ? 'Order Declined' : 'Order Canceled';

            // Sends an email explaining why an order is DECLINED or CANCELED
            $this->sendEmail($mailer, $subject, $user, 'emails/reason.html.twig', [
                'user' =>  $user,
                'message' => $data['message'],
                'idButton' => $idButton,
            ]);
        }
        if($idButton == 7){
            return $this->render('onlineorder/reason.html.twig', [
                'title' => "Decline Reason",
                'headtitle' => "Explain why you declined the order",
                'onlineorder' => $onlineorder,
                'form' => $form->createView(),
            ]);
        }else {
            return $this->render('onlineorder/reason.html.twig', [
                'title' => "Cancel Reason",
                'headtitle' => "Explain why the order is canceled",
                'onlineorder' => $onlineorder,
                'form' => $form->createView(),
            ]);
        }
    }

    /////////////////
    ///  METHODS  ///
    /////////////////

    /**
     * Shows the order as a pdf file in a new Tab
     * @Route("/{idonlineorder}/pdf", name="showOrderToPDF", methods={"GET"})
     * @param Onlineorder $onlineorder
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     */
    public function showOrderToPDF(Onlineorder $onlineorder){
        $html2pdf = new Html2Pdf();
        $html2pdf->writeHTML($onlineorder->getPdfContent());
        $html2pdf->output('order#'.$onlineorder->getIdonlineorder().'.pdf');
    }

    /**
     * Defines the type of orders to show according to their status
     * @param $status
     * @param ManagerRegistry $managerRegistry
     * @return object[]
     */
    public function switchStatus($status, ManagerRegistry $managerRegistry){
        $onlineorder = new Onlineorder();
        $realStatus = $status-2;

        if($realStatus>=0 && $realStatus<=6){
            return $managerRegistry->getRepository(Onlineorder::class)
                ->findByStatus($onlineorder->STATUSES[$realStatus]);
        }else if($realStatus == 7){
            return $managerRegistry->getRepository(Onlineorder::class)
                ->findUrgent(true);

        }else if ($realStatus == 8){
            return $managerRegistry->getRepository(Onlineorder::class)
                ->findPersonal(true);

        }
        return $managerRegistry->getRepository(Onlineorder::class)
            ->findAll();
    }

    /**
     * Say which statuss to apply to an order
     * @param int $idButton
     * @param Onlineorder $onlineorder
     * @param Request $req
     * @param \Swift_Mailer $mailer
     * @param string $msg
     */
    public function switchIdButton( int $idButton, Onlineorder $onlineorder, Request $req, \Swift_Mailer $mailer, string $msg){
        $onlineorder->setStatus($onlineorder->STATUSES[$idButton]);
        $req->query->set('onlineOrders', $idButton);
        if($idButton == 0 || $idButton == 2 || $idButton == 3){}
        else{
            $user = $onlineorder->getIduser();
            $onlineorder->setStatus($onlineorder->STATUSES[$idButton]);
            $req->query->set('onlineOrders', $idButton);
            $subject = 'Order is now '.$onlineorder->STATUSES[$idButton] ;

            // Sends an email saying the status of an order
            $this->sendEmail($mailer, $subject, $user, 'emails/validatedOrderedArrivedProcessed.html.twig', [
                'user' =>  $user,
                'message' => $msg,
            ]);
        }
    }

    /**
     * This function allows the sending of any kind of email
     *
     * @param \Swift_Mailer $mailer
     * @param string $subject
     * @param User $user
     * @param string $templateDir
     * @param array $parameters the parameter used in the rendering
     */
    public function sendEmail(\Swift_Mailer $mailer, string $subject, User $user, string $templateDir, array $parameters){
        $message = (new \Swift_Message($subject))
            ->setFrom('IOTThomasMoreDelivery@gmail.com')
            ->setTo( $user->getEmail())
            ->setBody(
                $this->renderView(
                    $templateDir,
                    $parameters),
                'text/html'
            );
        $mailer->send($message);
    }

    /**
     * Method used to create the whole order
     * @param Onlineorder $onlineorder
     * @param ArrayCollection $list
     */
    public function createFromOrder(User $user, Onlineorder $onlineorder, ArrayCollection $list){
        if($user->getRoles()[0] == 'ROLE_STUDENT'){
            $onlineorder->seturgent(false);
            $onlineorder->setPersonal(false);
        }

        //Create the online order so we can get its id
        $this->save($onlineorder);
        $html2pdf_content = $this->createPDF_toString($user, $onlineorder, $list);
        $onlineorder->setPdfContent($html2pdf_content);
        $this->save($onlineorder);
    }

    /**
     * Returns only the content of the PDF file
     * @param User $user
     * @param Onlineorder $order
     * @param ArrayCollection $list
     * @return string
     */
    public function createPDF_toString(User $user, Onlineorder $order, ArrayCollection $list){
        $yes = 'YES'; $no = 'NO';
        $totalPrice=0;
        $totalProduct=0;
        $role = ltrim($user->getRoles()[0], 'ROLE_');
        $stringList = "";
        foreach ($list as $item){
            $totalProduct+=$item->getQuantity();
            $totalPrice+=($item->getPrice()*$item->getQuantity());
        }
        foreach ($list as $article){
            $stringList=$stringList."<tr>
                        <td>".$article->getName()."</td>
                        <td>".$article->getPrice()."</td>
                        <td>".$article->getQuantity()."</td>
                        <td>".$article->getProviderName()."</td></tr>";
        }

        $style = "<style>
                    table, th, td {border: 1px solid black; border-collapse: collapse;}
                    th, td {padding: 5px; text-align: left;}
                  </style>";

        $content = ' <h1>Order #'.$order->getIdonlineorder().'</h1>';
        if($order->geturgent() == true){
            $content = $content.'<br><b>Urgent : YES </b>';
        }else { $content = $content.'<br><b>Urgent : NO </b>'; }

        if ($order->getPersonal() == true){
            $content = $content.'<br><b>Personal : YES  </b>';
        }else{ $content = $content.'<br><b>Personal : NO  </b>'; }

        $content = $content
            .'<br><b>Creation date : </b>'.$order->getCreationdate()->format('Y-m-d H:i:s')
            .'<br><b>Client : </b>'.$user->getFirstname().' '.$user->getLastname()
            .'<br><b>Email address : </b>'.$user->getEmail()
            .'<br><b>Status : </b>'.$role

            .'<h2>Subject</h2><p>'.$order->getDescription()

            .'</p><h2>Articles</h2><table style="width:100%"><thead><tr><th>Article\'s name</th><th>Price for 1</th><th>Quantity</th><th>
            Provider</th></tr></thead><tbody>'.$stringList.'<tr><td><b>TOTAL:</b></td><td><b>'.$totalPrice.'€ </b></td><td>
            <b>Number of items : '.$totalProduct.'</b></td></tr></tbody></table>';
        return $style.$content;
    }

    /**
     * Method to save an object in the database
     * @param $entity
     */
    public function save($entity){
        $entityManager=$this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }
}
