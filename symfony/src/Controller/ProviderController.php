<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Grade;
use App\Entity\Provider;
use App\Entity\User;
use App\Form\CommentType;
use App\Form\ProviderType;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/provider")
 */
class ProviderController extends SecurityController
{

    ////////////////
    ///  ROUTES  ///
    ////////////////

    /**
     * @Route("/", name="provider_index", methods={"GET"})
     */
    public function index(): Response
    {
        $providers = $this->getDoctrine()
            ->getRepository(Provider::class)
            ->findAll();

        return $this->render('provider/index.html.twig', [
            'providers' => $providers,
        ]);
    }

    /**
     * @Route("/new", name="provider_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $provider = new Provider();
        $error="";
        $listArticleExisting = $this->getDoctrine()->getRepository(Provider::class)->findAll();
        $form = $this->createForm(ProviderType::class, $provider);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $provider->setReliable(true);
            $this->save($provider);

            if($this->providerExistsInDatabase($provider)){
                $error="ERROR: the article already exists with a different name. Check out your database.";
                return $this->render('provider/new.html.twig', [
                    'provider' => $provider,
                    'error' => $error,
                    'list' => $listArticleExisting,
                    'form' => $form->createView(),
                ]);
            }
            return $this->redirectToRoute('provider_new');
        }

        return $this->render('provider/new.html.twig', [
            'provider' => $provider,
            'error' => $error,
            'list' => $listArticleExisting,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{iduser}/{idprovider}", name="provider_show", methods={"GET", "POST"})
     */
    public function show(Request $request, Provider $provider, User $user): Response
    {
        $count=0;
        $value=null;
        $previousGrade=null;
        $grade=$this->getDoctrine()->getRepository(Grade::class)->findOneBy(['iduser'=>$user->getIduser(), 'idprovider' => $provider->getIdprovider()]);

        if($request->request->has("rate")){
            while($count<6 && !$value){
                $value=$request->request->get("rate");
                $this->rateProvider($provider, $user, $value);
            }
        }
        if($grade){
            $previousGrade=$grade->getRate();
        }
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $now = new \DateTime('NOW');

            $commentExists=$this->getDoctrine()->getRepository(Comment::class)->findOneBy(['idprovider'=>$provider->getIdprovider(),
                'iduser'=>$user->getIduser()]);
            if($commentExists){
                $commentExists->setContent($comment->getContent());
                $commentExists->setPostdate($now);
                $this->save($commentExists);
            }else{
                $comment->setPostdate($now);
                $comment->setIduser($user);
                $comment->setIdprovider($provider);
                $this->save($comment);
            }
        }

        return $this->render('provider/show.html.twig', [
            'provider' => $provider,
            'previousGrade' => $previousGrade,
            'gradeAmount' => $this->getGradesAmount($provider),
            'form'=>$form->createView(),

        ]);
    }

    /**
     * @Route("/{idprovider}", name="showAllComments", methods={"GET", "POST", "DELETE"})
     * @param Request $request
     * @param Provider $provider
     * @return Response
     */
    public function showAllComments(Request $request, Provider $provider){
        $users=$this->getDoctrine()->getRepository(User::class)->findAll();
        $comments=$this->getDoctrine()->getRepository(Comment::class)->findBy(['idprovider'=>$provider->getIdprovider()]);

        return $this->render('comment/show.html.twig', [
            'comments'=>$comments,
            'users'=>$users,
            'provider'=>$provider
        ]);
    }


    /**
     * @Route("/{idprovider}", name="provider_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Provider $provider): Response
    {
        if ($this->isCsrfTokenValid('delete'.$provider->getIdprovider(), $request->request->get('_token'))) {
            //Delete grades
            $grades=$this->getDoctrine()->getRepository(Grade::class)->findBy(["idprovider"=>$provider->getIdprovider()]);
            foreach($grades as $grade){
                $this->remove($grade);
            }
            //Delete comments
            $comments=$this->getDoctrine()->getRepository(Comment::class)->findBy(["idprovider"=>$provider->getIdprovider()]);
            foreach($comments as $comment){
                $this->remove($comment);
            }
            $this->remove($provider);
        }

        return $this->redirectToRoute('provider_new');
    }

    /**
     * @Route("/{idprovider}", name="provider_changeReliability", methods={"GET"})
     * @param Provider $provider
     */
    public function changeReliablilty(Provider $provider){
        $provider->setReliable(!$provider->getReliable());
        $this->save($provider);
        return $this->redirectToRoute('provider_new');
    }



    /////////////////
    ///  METHODS  ///
    /////////////////

    /**
     * Compares the names of the provider in parameter and the ones in the database without letter case nor blank spaces
     * If two names match, one of the provider is deleted
     * @param Provider $provider
     * @return bool
     */
    public function providerExistsInDatabase(Provider $provider){
        $repository=$this->getDoctrine()->getRepository(Provider::class);
        $allProviders=$repository->findAll();
        $count=0;
        foreach ($allProviders as $existing){
            $myProv=$repository->findOneBy(['idprovider' => $provider->getIdprovider()]);
            if($myProv){
                $existingName = strtolower(str_replace(' ','', $existing->getProviderName()));
                $newName = strtolower(str_replace(' ','', $myProv->getProviderName()));
                if($existingName==$newName){
                    $count+=1;
                }
            }
            if($count>1){
                $this->remove($provider);
                return true;
            }
        }
        return false;
    }

    public function rateProvider(Provider $provider, User $user, $grade ){

        $db_grade=$this->getDoctrine()->getRepository(Grade::class)->findOneBy(["idprovider"=>$provider->getIdprovider(), "iduser"=>$user->getIduser()]);

        if($db_grade){
            $db_grade->setRate($grade);
            $this->save($db_grade);
        }else{
            $newGrade = new Grade();
            $newGrade->setIduser($user);
            $newGrade->setIdprovider($provider);
            $newGrade->setRate($grade);
            $this->save($newGrade);
        }

        $countSum = $this->getGradesSum($provider);
        $countUser = $this->getGradesAmount($provider);
        $provider->setGlobalgrade(round($countSum/$countUser, 2));
        $this->save($provider);

    }

    public function getGradesSum(Provider $provider){
        $count=0;
        $repository=$this->getDoctrine()->getRepository(Grade::class)->findBy(["idprovider"=>$provider->getIdprovider()]);
        foreach($repository as $grade){
            $count+=$grade->getRate();
        }
        return $count;
    }

    public function getGradesAmount(Provider $provider){
        $repository=$this->getDoctrine()->getRepository(Grade::class)->findBy(["idprovider"=>$provider->getIdprovider()]);
        return count($repository);
    }

    /**
     * Method to save an object in the database
     * @param $entity
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function save($entity){
        try{
            $entityManager=$this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();
        }catch(UniqueConstraintViolationException | PDOException $e){
            return $this->redirectToRoute("provider_new");
        }

    }

    /**
     * Method to remove an object from the database
     * @param $entity
     */
    public function remove($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($entity);
        $entityManager->flush();
    }
}
