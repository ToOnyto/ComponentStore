<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Provider;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use App\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comment")
 */
class CommentController extends SecurityController
{

    ////////////////
    ///  ROUTES  ///
    ////////////////

    /**
     * @Route("/{idprovider}", name="showAllComments", methods={"GET", "POST", "DELETE"})
     * @param int $idprovider
     * @return Response
     */
    public function showAllComments( int $idprovider){
        $provider=$this->getDoctrine()->getRepository(Provider::class)->findOneBy(['idprovider'=>$idprovider]);
        $users=$this->getDoctrine()->getRepository(User::class)->findAll();
        $comments=$this->getDoctrine()->getRepository(Comment::class)->findBy(['idprovider'=>$provider->getIdprovider()]);
        return $this->render('comment/show.html.twig', [
            'comments'=>$comments,
            'users'=>$users,
            'provider'=>$provider
        ]);
    }

    /**
     * @Route("/{idprovider}/{idcomment}", name="comment_delete", methods={"DELETE", "GET", "POST"})
     * @Entity("provider", expr="repository.find(idprovider)")
     */
    public function delete(Request $request, Provider $provider, Comment $comment): Response
    {
        if ($this->isCsrfTokenValid('delete'.$comment->getIdcomment(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('showAllComments', ['idprovider'=> $provider->getIdprovider()]);
    }

    /////////////////
    ///  METHODS  ///
    /////////////////

    /**
     * Method to save an object in the database
     * @param $entity
     */
    public function save($entity){
        $entityManager=$this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }
}
