<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * @Route("/user")
 */
abstract class SecurityController extends AbstractController {

    protected function isSelfUser(User $user){
        if(is_null($this->getUser())){
            return false;
        }
        return $user == $this->getUser();
    }

    protected function isBanned(User $user){
        if( is_null($this->getUser())){
            return false;
        }
        return $user == $this->getUser()->getBanned();
    }

    protected function isAccountActivated(User $user){
        if(!$this->getUser()){
            return false;
        }
        return $user == $this->getUser()->getActivated();
    }

    /**
     * Method to avoid illegal access through URL
     * @param User $user
     */
    protected function denyAccessIfNotCurrentUser(User $user){
        if($user!=$this->getUser()){
            throw new AccessDeniedException();
        }
    }

    /**
     * Method to avoid illegal access through URL if the account is not activated
     * @param User $user
     */
    protected function denyAccssIfNotActivated(User $user){
        if($user->getActivated() != true){
            throw new AccessDeniedException();
        }
    }

}
