<?php

namespace App\Controller;

use App\Entity\Evaluation;
use App\Entity\User;
use App\Form\PasswordType;
use App\Form\UserEditType;
use App\Form\UserType;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @Route("/user")
 */
class UserController extends SecurityController
{

    ////////////////
    ///  ROUTES  ///
    ////////////////

    /**
     * Requires ADMIN_ROLE
     *
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $ban = true;
        $role = '["ROLE_STUDENT"]';

        $value = $request->query->get('users');

        switch ($value){
            case 2 :
                $users = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findByRole($role);
                break;
            case 3 :
                $role = '["ROLE_TEACHER"]';
                $users = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findByRole($role);
                break;
            case 4 :
                $role = '["ROLE_ADMIN"]';
                $users = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findByRole($role);
                break;
            case 5 :
                $role = '["ROLE_SUPER_ADMIN"]';
                $users = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findByRole($role);
                break;
            case 6 :
                $users = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findByBan($ban);
                break;
            case 7 :
                $ban = false;
                $users = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findByBan($ban);
                break;
            default:
                $users = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->findAll();
                break;
        }

        return $this->render('user/index.html.twig', [
            'title' => 'User Index',
            'headtitle' => 'View all the users list',
            'users' => $users,
        ]);
    }


    /**
     * @Route("/{iduser}", name="user_show", methods={"GET", "POST"})
     */
    public function show(User $user_rated, Request $request)
    {
        $user_rater=$this->getUser();
        $this->denyAccssIfNotActivated($user_rater);
        $count=0;
        $value=null;
        $lastEvaluation=null;
        //Check if the list of article has been cleared for this session
        $list=$this->container->get('session')->get('list');
        if($list){
            $list->clear();
        }
        // Accessible by the user and the ADMIN and higher
        if( !$this->isGranted('ROLE_ADMIN') && !$this->isSelfUser($user_rater)){
            throw new AccessDeniedException();
        }

        if($request->request->has("rate")){
            while($count<6 && !$value){
                $value=$request->request->get("rate");
                $this->evaluateUser($user_rated, $user_rater, $value);
            }
        }

        $formerEval=$this->getDoctrine()->getRepository(Evaluation::class)->findOneBy(["idrater"=>$user_rater->getIduser(), "idrated"=>$user_rated->getIduser()]);

        if($formerEval){
            $lastEvaluation=$formerEval->getRate();
        }

        return $this->render('user/show.html.twig', [
            'title' => "My Account",
            'user' => $user_rated,
            'last_evaluation'=>$lastEvaluation,
            'nbVotes'=>$this->getEvaluationsAmount($user_rated)
        ]);
    }

    /**
     * @Route("/{iduser}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user)
    {
        $this->denyAccssIfNotActivated($user);
        // Accessible by the user and SUPER ADMIN
        if( !$this->isGranted('ROLE_SUPER_ADMIN') && !$this->isSelfUser($user)){
            throw new AccessDeniedException();
        }

        $form = $this->createForm(UserEditType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em=$this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('user_show', [
                'iduser' => $user->getIduser(),
            ]);
        }

        return $this->render('user/edit.html.twig', [
            'title' => 'Edit',
            'headtitle' => 'Edit your information',
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{iduser}/edit/Password", name="user_edit_password", methods={"GET","POST"})
     */
    public function editPassword(Request $request, User $user, UserPasswordEncoderInterface $encoder)
    {
        $this->denyAccssIfNotActivated($user);
        $this->denyAccessIfNotCurrentUser($user);

        $session=$this->container->get('session');
        if(!$session->get('formerUser')){
            $session->set('formerUser', $user);
        }
        $formerUser=$session->get('formerUser');
        $form = $this->createForm(PasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $psw1=$form->get("password")->getData();
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);

            if($encoder->isPasswordValid($formerUser,$form->get('currentPassword')->getData())){

                $user->setPassword($encoder->encodePassword($user, $psw1));
                $session->remove('formerUser');

            }else{
                $error="Unvalid password";
                $user->setPassword( $formerUser->getPassword());
                $this->save($user);
                return $this->render('user/edit_Password.html.twig', [
                    'title' => 'Edit password',
                    'headtitle' => 'Change your password',
                    'form' => $form->createView(),
                    'user' => $user,
                    'error' => $error ,
                ]);
            }

            $this->save($user);
            return $this->redirectToRoute('user_index');

        }
        return $this->render('user/edit_Password.html.twig', [
            'title' => 'Edit password',
            'headtitle' => 'Change your password',
            'form' => $form->createView(),
            'user' => $user
        ]);
    }

    /**
     * @Route("/{iduser}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user)
    {
        $this->denyAccssIfNotActivated($user);
        // Accessible by the user and SUPER ADMIN
        if( !$this->isGranted('ROLE_SUPER_ADMIN') && !$this->isSelfUser($user)){
            throw new AccessDeniedException();
        }

        $submittedToken = $request->request->get('_token');

        if ($this->isCsrfTokenValid('delete'.$user->getIduser(), $submittedToken)) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }
        return $this->redirectToRoute('user_index');
    }

    /**
     * @Route("/{iduser}/ban", name="user_ban")
     */
    public function ban(\Swift_Mailer $mailer, Request $request, User $user){

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $submittedToken = $request->request->get('_token');

        $isBanned = $user->getBanned();

        if ( !$isBanned && $this->isCsrfTokenValid('ban'.$user->getIduser(), $submittedToken)) {
            $now = new \DateTime('NOW');

            // Ban for 500 years if super admin or 12 days if admin
            if($this->isGranted('ROLE_SUPER_ADMIN')){
                $expiration = new \DateTime('NOW +500 years');
            }
            elseif ($this->isGranted('ROLE_ADMIN')){
                $expiration = new \DateTime('NOW +14 days');
            }

            $user->setBanned(true);
            $user->setBan_date($now);
            $user->setBan_end_date($expiration);

            $this->save($user);

            // Sends an email when a user is banned
            $this->sendEmail($mailer, 'Banned', $user, 'emails/banned.html.twig', [
                'user' => $user ]);

            return $this->redirectToRoute('user_show', [
                'iduser' => $user->getIduser(),
            ]);

        }
        elseif ( $isBanned && $this->isCsrfTokenValid('ban'.$user->getIduser(), $submittedToken)) {
            $user->setBanned(false);
            $user->setBan_date(null);
            $user->setBan_end_date( null);
            $this->save($user);
        }

        return $this->redirectToRoute('user_show', [
            'iduser' => $user->getIduser(),
        ]);
    }

    /**
     * @Route("/{iduser}/set_admin", name="user_set_admin")
     */
    public function setAdmin(Request $request, User $user){

        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');

        $submittedToken = $request->request->get('_token');
        $roles = $user->getRoles();

        if ( $roles[0] != 'ROLE_ADMIN' && $this->isCsrfTokenValid('set_admin'.$user->getIduser(), $submittedToken)) {
            $user->setRoles(array($user->ROLES[2]));
            $this->save($user);
        } elseif ($roles[0] == 'ROLE_ADMIN' && $this->isCsrfTokenValid('unset_admin'.$user->getIduser(), $submittedToken)){

            if(strpos($user->getEmail(), "@thomasmore.be") ){
                $user->setRoles(array($user->ROLES[1]));
            }else{
                $user->setRoles(array($user->ROLES[0]));

            }
            $this->save($user);
        }

        return $this->redirectToRoute('user_show', [
            'iduser' => $user->getIduser(),
        ]);
    }


    /////////////////
    ///  METHODS  ///
    /////////////////

    /**
     * This function allows the sending of any kind of email
     *
     * @param \Swift_Mailer $mailer
     * @param string $subject
     * @param User $user
     * @param string $templateDir
     * @param array $parameters the parameter used in the rendering
     */
    public function sendEmail(\Swift_Mailer $mailer, string $subject, User $user, string $templateDir, array $parameters){
        $message = (new \Swift_Message($subject))
            ->setFrom('IOTThomasMoreDelivery@gmail.com')
            ->setTo( $user->getEmail())
            ->setBody(
                $this->renderView(
                    $templateDir,
                    $parameters),
                'text/html'
            );
        $mailer->send($message);
    }


    public function evaluateUser(User $user_rated, User $user_rater, $grade ){

        $db_grade=$this->getDoctrine()->getRepository(Evaluation::class)->findOneBy(["idrated"=>$user_rated->getIduser(), "idrater"=>$user_rater->getIduser()]);

        if($db_grade){
            $db_grade->setRate($grade);
            $this->save($db_grade);
        }else{
            $newEvaluation = new Evaluation();
            $newEvaluation->setIdrater($user_rater);
            $newEvaluation->setIdrated($user_rated);
            $newEvaluation->setRate($grade);
            $this->save($newEvaluation);
        }

        $countSum = $this->getEvaluationsSum($user_rated);
        $countUser = $this->getEvaluationsAmount($user_rated);
        $user_rated->setEvaluation(round($countSum/$countUser, 2));
        $this->save($user_rated);
    }

    public function getEvaluationsSum(User $user_rated){
        $count=0;
        $repository=$this->getDoctrine()->getRepository(Evaluation::class)->findBy(["idrated"=>$user_rated->getIduser()]);
        foreach($repository as $grade){
            $count+=$grade->getRate();
        }
        return $count;
    }

    public function getEvaluationsAmount(User $user_rated){
        $repository=$this->getDoctrine()->getRepository(Evaluation::class)->findBy(["idrated"=>$user_rated->getIduser()]);
        return count($repository);
    }



    /**
     * Method to save an object in the database
     * @param $entity
     */
    public function save($entity){
        $entityManager=$this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }
}













