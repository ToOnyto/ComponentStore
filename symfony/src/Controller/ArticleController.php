<?php

namespace App\Controller;
use App\Entity\Article;
use App\Entity\Provider;
use App\Entity\TemporaryArticle;
use App\Entity\User;
use App\Form\ArticleType;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * @Route("/article")
 */
class ArticleController extends SecurityController
{

    ////////////////
    ///  ROUTES  ///
    ////////////////

    /**
     * @Route("/new", name="article_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $article = new Article();
        $error='';
        $listArticleExisting = $this->getDoctrine()->getRepository(Article::class)->findAll();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->save($article);
            if($this->articleExistsInDatabase($article)){
                $error="ERROR: the article already exists with a different name. Check out your database.";
                return $this->render('article/new.html.twig', [
                    'article' => $article,
                    'error' => $error,
                    'list' => $listArticleExisting,
                    'form' => $form->createView(),
                ]);
            }

            return $this->redirectToRoute('article_new');
        }

        return $this->render('article/new.html.twig', [
            'article' => $article,
            'error' => $error,
            'list' => $listArticleExisting,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idarticle}", name="article_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Article $article): Response
    {
        if ($this->isCsrfTokenValid('delete'.$article->getIdarticle(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($article);
            $entityManager->flush();
        }

        return $this->redirectToRoute('article_new');
    }

    /**
     * @Route("/{iduser}/selectArticle", name="articleList", methods={"GET","POST"})
     */
    public function addArticle(Request $request, User $user): Response
    {
        $this->denyAccssIfNotActivated($user);
        $this->denyAccessIfNotCurrentUser($user);

        $totalNumber=0;
        $totalPrice=0;
        $db_hasProviders=false;
        $db_hasArticles=false;
        //Form options initialized
        $provider_option=$this->getProviderOptionForm();
        $article_option=$this->getArticleOptionForm();
        if(!$provider_option->isEmpty()){
            $db_hasProviders=true;
        }
        if(!$article_option->isEmpty()){
            $db_hasArticles=true;
        }
        //Save
        $session = new Session();
        $list=$session->get('list');
        //Creation of the form
        $tempArt = new TemporaryArticle();
        $form=$this->createProviderFormField($tempArt, $article_option, $provider_option);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newProvider = $form->get('newProvider')->getData();
            $newArticle = $form->get('newArticle')->getData();
            $tempArt=$form->getData();
            if(!$newProvider) {
                $newProvider = $form->get('providerName')->getData()->getProviderName();
            }
            if(!$newArticle) {
                $newArticle = $form->get('name')->getData()->getArticleName();
            }
            $tempArt->setName($newArticle);
            $tempArt->setProviderName($newProvider);
            $list->add($tempArt);
            foreach ($list as $item){
                $totalNumber+=$item->getQuantity();
                $totalPrice+=($item->getPrice()*$item->getQuantity());
            }
            return $this->render('article/makeList.html.twig', [
                'list' => $list,
                'user' => $user,
                'hasArticles' => $db_hasArticles,
                'hasProviders' => $db_hasProviders,
                'totalNumber'=> $totalNumber,
                'totalPrice' => $totalPrice,
                'form' => $form->createView(),
            ]);
        }
        foreach ($list as $item){
            $totalNumber+=$item->getQuantity();
            $totalPrice+=($item->getPrice()*$item->getQuantity());
        }
        return $this->render('article/makeList.html.twig', [
            'list' => $session->get('list'),
            'user' => $user,
            'hasArticles' => $db_hasArticles,
            'hasProviders' => $db_hasProviders,
            'totalNumber'=> $totalNumber,
            'totalPrice' => $totalPrice,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{iduser}/removeArticle/{article}", name="article_removeFromList", methods={"GET","POST"})
     */
    public function removeArticleFromList(string $article, User $user): Response
    {
        $list = $this->container->get('session')->get('list');
        $found=false;
        foreach($list as $art){
            if($art->getName()==$article && !$found){
                $index=$list->indexOf($art);
                $list->remove($index);
                $found=true;
            }
        }

        return $this->redirectToRoute('articleList', array('iduser' => $user->getIduser()));
    }


    /////////////////
    ///  METHODS  ///
    /////////////////

    /**
     * Creation of the special form
     * @param TemporaryArticle $tempArt
     * @param ArrayCollection $article_option
     * @param ArrayCollection $provider_option
     * @return \Symfony\Component\Form\FormInterface
     */
    public function createProviderFormField(TemporaryArticle $tempArt, ArrayCollection $article_option, ArrayCollection $provider_option){
        $form = $this->createFormBuilder($tempArt)
            ->add('price', MoneyType::class)
            ->add('quantity', NumberType::class);
        if($article_option->isEmpty()){
            $form->add('newArticle', TextType::class, array('mapped' => false));
        }else{
            $form->add('name', ChoiceType::class, ['mapped' =>false,'choices' => $article_option,])
                ->add('I_want_another_article', CheckboxType::class, array('mapped' => false, 'required'=>false))
                ->add('newArticle', TextType::class, array('mapped' => false));
        }
        if($provider_option->isEmpty()){
            $form->add('newProvider', TextType::class, array('mapped' => false));
        }else{
            $form->add('providerName', ChoiceType::class, ['mapped' =>false,'choices' => $provider_option,])
                ->add('I_want_another_provider', CheckboxType::class, array('mapped' => false, 'required'=>false))
                ->add('newProvider', TextType::class, array('mapped' => false));
        }
        return $form->getForm();
    }

    /**
     * Creates options for provider form
     * @return ArrayCollection
     */
    public function getProviderOptionForm(){
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $warning = "    -----------⚠️ Not reliable ⚠️";
        $articles = $qb->select(array('p'))
            ->from(Provider::class, 'p')
            ->orderBy('p.providername', 'ASC')
            ->getQuery()
            ->getResult();
        $option=new ArrayCollection();
        foreach ($articles as $name){
            if($name->getReliable()){
                $option->set($name->getProviderName(), $name);
            }else{
                $option->set($name->getProviderName().$warning, $name);

            }
        }
        return $option;
    }

    /**
     * Creates options for article form
     * @return ArrayCollection
     */
    public function getArticleOptionForm(){
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $articles = $qb->select(array('a'))
            ->from(Article::class, 'a')
            ->orderBy('a.articlename', 'ASC')
            ->getQuery()
            ->getResult();
        $option=new ArrayCollection();
        foreach ($articles as $name){
            $option->set($name->getArticleName(), $name);
        }
        return $option;
    }

    /**
     * Compares the names of the article in parameter and the ones in the database without letter case nor blank spaces
     * If two names match, one of the article is deleted
     * @param Article $article
     * @return bool
     */
    public function articleExistsInDatabase(Article $article){
        $repository=$this->getDoctrine()->getRepository(Article::class);
        $allArticles=$repository->findAll();
        $count=0;
        foreach ($allArticles as $existing){
            $myArt=$repository->findOneBy(['idarticle' => $article->getIdarticle()]);
            if($myArt){
                $existingName = strtolower(str_replace(' ','', $existing->getArticleName()));
                $newName = strtolower(str_replace(' ','', $myArt->getArticleName()));
                if($existingName==$newName){
                    $count+=1;
                }
            }
            if($count>1){
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($article);
                $entityManager->flush();
                return true;
            }
        }
        return false;
    }

    /**
     * Method to save an object in the database
     * @param $entity
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function save($entity){
        try{
            $entityManager=$this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();
        }catch(UniqueConstraintViolationException | PDOException $e){
            return $this->redirectToRoute("article_new");
        }
    }
}
