<?php

namespace App\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{

    ////////////////
    ///  ROUTES  ///
    ////////////////

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('index/index.html.twig', [
            'title' => "Home",
        ]);
    }

    /**
     * @Route("/about", name="about")
     */
    public function about()
    {
        return $this->render('index/about.html.twig', [
            'title' => "About",
            'headtitle' => "About this website"
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact()
    {
        return $this->render('index/contact.html.twig', [
            'title' => "Contact",
            'headtitle' => "Contact"
        ]);
    }

    /////////////////
    ///  METHODS  ///
    /////////////////

    /**
     * Initializes a session for the user
     */
    public function initializeSession(){
        if(!$this->container->get('session')->isStarted()){
            $session = new Session();
            $session->start();
        }
        if(!$this->container->get('session')->get('list')){
            $this->container->get('session')->set('list', new ArrayCollection());
        }
    }
}
