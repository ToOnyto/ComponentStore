<?php

namespace App\Controller;

use App\Entity\Usager;
use App\Entity\User;
use App\Form\UserType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use \Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthenticationController extends SecurityController
{

    ////////////////
    ///  ROUTES  ///
    ////////////////

    /**
     * @Route("/registration", name="user_registration", methods={"GET","POST"})
     */
    public function registration(\Swift_Mailer $mailer, Request $request,
                           UserPasswordEncoderInterface $encoder): Response
    {
        $user = new User();

        $form = $this->createForm( UserType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            // hashing passwords
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            //Analyses the email
            $emailValid = $this->emailAdvisor($form->get('email')->getData(), $user);
            $user->setBanned(false);
            // Generates a 6 chars code
            $random = substr(md5(rand()), 0, 6);
            $user->setSecurityCode($random);
            $user->setActivated(false);


            if( $emailValid != -1){
                // Sends an email with a safe code to activate a user account
                $this->sendEmail($mailer, 'Activate your account', $user,
                    'emails/activation.html.twig', [
                        'user' => $user,
                        'security_code' => $user->getSecurityCode(),
                    ]);

                $this->save($user);
            }

            return $this->redirectToRoute('user_login');
        }

        return $this->render('authentication/registration.html.twig', [
            'user' => $user,
            'title' => 'Sign Up',
            'headtitle' => 'Sign Up',
            'signupForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/login", name="user_login" )
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('authentication/login.html.twig', [
            'title' => 'Log In',
            'headtitle' => 'Log In',
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }

    /**
     * @Route("/logout", name="user_logout")
     */
    public function logout(){}

    /**
     * @Route("/afterlogin", name="after_login")
     */
    public function after_login(){
        $this->initializeSession();
        $user = $this->getUser();
        $now = new \DateTime('NOW');

        if(!$this->isAccountActivated($user)){
            return $this->redirectToRoute('account_activate', [
                'iduser' => $user->getIduser(),
            ]);
        }

        if($this->isBanned($user)){
            if($now > $user->getBan_end_date()){

                $user->setBanned(false);
                $user->setBan_date(null);
                $user->setBan_end_date( null);
                $this->save($user);

                return $this->redirectToRoute('user_show', [
                    'iduser' => $user->getIduser(),
                ]);
            }
            else{
                return $this->redirectToRoute('user_logout');
            }
        }
        else{
            return $this->redirectToRoute('user_show', [
                'iduser' => $user->getIdUser(),
            ]);
        }
    }


    /**
     * @Route("/passwordForgotten", name="password_forgotten")
     */
    public function passwordForgotten(Request $request, \Swift_Mailer $mailer){

        $form = $this->createFormBuilder()
            ->add('email')
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $allUsers = $this->getDoctrine()->getRepository(User::class)->findAll();

            foreach ( $allUsers as $user ){
                if( $user->getEmail() == $form->get('email')->getData()){
                    // Generates a 6 chars code
                    $random = substr(md5(rand()), 0, 6);
                    $user->setSecurityCode($random);

                    $this->save($user);

                    // Send an email with a random safe code
                    $this->sendEmail($mailer, 'Password forgotten', $user,
                        'emails/passwordForgottenCode.html.twig',
                        [ 'user' => $user,
                            'security_code' => $random,
                        ]);
                }
            }

            return $this->redirectToRoute('check_password_forgotten');
        }

        return $this->render('authentication/passwordForgotten.html.twig', [
            'title' => 'PasswordForgotten',
            'headtitle' => 'Did you forgot your password ?',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/checkForgot", name="check_password_forgotten")
     */
    public function checkPasswordForgotten(Request $request, \Swift_Mailer $mailer, UserPasswordEncoderInterface $encoder)
    {

        $form = $this->createFormBuilder()
            ->add('email')
            ->add('security_code')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $allUsers = $this->getDoctrine()->getRepository(User::class)->findAll();

            foreach ($allUsers as $user) {
                if ($user->getEmail() == $form->get('email')->getData()) {
                    if ($user->getSecurityCode() == $form->get('security_code')->getData()) {

                        // Generates a 10 chars password
                        $random = substr(md5(rand()), 0, 10);

                        $user->setPassword($encoder->encodePassword($user, $random));
                        $this->save($user);
                        // Sends an email with the new password
                        $this->sendEmail($mailer, 'Password changed', $user,
                            'emails/newPassword.html.twig', [
                                'user' => $user,
                                'password' => $random
                            ]);
                    }
                }

                return $this->redirectToRoute('user_login');
            }
        }
        return $this->render('authentication/checkPasswordForgotten.html.twig', [
            'title' => 'PasswordForgotten',
            'headtitle' => 'Enter the code received by email',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/logout_message", name="user_logout_message")
     */
    public function logout_message(){
        $message = "You are banned";
        return $this->redirectToRoute('index');
    }

    /**
     * @Route("/account_activation/{iduser}", name="account_activate")
     */
    public function activateAccount(Request $request){
        $user = $this->getUser();
        $form = $this->createFormBuilder($user)
            ->add('security_code', TextType::class, [
                'mapped' => false
            ])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $codeDB = $user->getSecurityCode();
            $codeEntered = $form['security_code']->getData();

            if( $codeEntered == $codeDB ){
                $user->setActivated(true);
                $this->save($user);

                return $this->redirectToRoute('user_show', [
                    'iduser' => $user->getIdUser(),
                ]);
            }
            else{ $this->redirectToRoute('index'); }
        }

        return $this->render('authentication/accountActivation.html.twig', [
            'user' => $user,
            'title' => 'Activate Account',
            'headtitle' => 'Activate your account',
            'code' => $user->getSecurityCode(),
            'accountActivationForm' => $form->createView(),
        ]);
    }


    /////////////////
    ///  METHODS  ///
    /////////////////


    /**
     * This function says the role of users depending on their emails
     *
     * @param String $email
     * @return string
     */
    public function emailAdvisor(String $email, User $user){
        //Set ROLE_STUDENT
        if(strpos($email, "@student.thomasmore.be")){
            $user->setRoles(array($user->ROLES[0]));
            return 0;
        }
        //Set ROLE_TEACHER
        else if(strpos($email, "@thomasmore.be")){
            $user->setRoles((array($user->ROLES[1])));
            return 1;
        }
        //  SET SUPERADMIN
        //  To create a superadmin with teacher email change this line
       //else if(strpos($email, "@")){
       //    $user->setRoles((array($user->ROLES[3])));
       //   return 3;
       // }
        else{
            return -1;
        }
    }

    /**
     * This function allows the sending of any kind of email
     *
     * @param \Swift_Mailer $mailer
     * @param string $subject
     * @param User $user
     * @param string $templateDir
     * @param array $parameters the parameter used in the rendering
     */
    public function sendEmail(\Swift_Mailer $mailer, string $subject, User $user, string $templateDir, array $parameters){
        $message = (new \Swift_Message($subject))
            ->setFrom('IOTThomasMoreDelivery@gmail.com')
            ->setTo( $user->getEmail())
            ->setBody(
                $this->renderView(
                    $templateDir,
                    $parameters),
                'text/html'
            );
        $mailer->send($message);
    }

    /**
     * Method to check if the session has been created
     */
    public function initializeSession(){
        if(!$this->container->get('session')->isStarted()){
            $session = new Session();
            $session->start();
        }
        if(!$this->container->get('session')->get('list')){
            $this->container->get('session')->set('list', new ArrayCollection());
        }
    }

    /**
     * Method to save an object in the database
     * @param $entity
     */
    public function save($entity){
        $entityManager=$this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }
}
