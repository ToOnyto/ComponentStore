<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Provider
 *
 * @ORM\Table(name="provider", uniqueConstraints={@ORM\UniqueConstraint(name="idProvider_UNIQUE", columns={"idProvider"}), @ORM\UniqueConstraint(name="idComment_UNIQUE", columns={"idComment"}), @ORM\UniqueConstraint(name="providerName_UNIQUE", columns={"providerName"})})
 * @ORM\Entity
 */
class Provider
{
    /**
     * @var int
     *
     * @ORM\Column(name="idProvider", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idprovider;

    /**
     * @var string
     *
     * @ORM\Column(name="providerName", type="string", length=30, nullable=false)
     */
    private $providername;

    /**
     * @var bool
     *
     * @ORM\Column(name="reliable", type="boolean", nullable=false, options={"default"="1"})
     */
    private $reliable;

    /**
     * @var float|null
     *
     * @ORM\Column(name="globalGrade", type="float", nullable=true, options={"default"="NULL"})
     */
    private $globalgrade = null;

    /**
     * @var int
     *
     * @ORM\Column(name="idComment", type="integer", nullable=true, options={"default"="NULL"})
     */
    private $idcomment;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Article", inversedBy="idprovider")
     * @ORM\JoinTable(name="provides",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idProvider", referencedColumnName="idProvider")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="idArticle", referencedColumnName="idArticle")
     *   }
     * )
     */
    private $idarticle;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idarticle = new ArrayCollection();
        $this->idusers = new ArrayCollection();
    }

    public function getIdprovider(): ?int
    {
        return $this->idprovider;
    }

    public function getProvidername(): ?string
    {
        return $this->providername;
    }

    public function setProvidername(string $providername): self
    {
        $this->providername = $providername;

        return $this;
    }

    public function getReliable(): ?bool
    {
        return $this->reliable;
    }

    /**
     * @param bool $reliable
     * @return Provider
     */
    public function setReliable(bool $reliable): self
    {
        $this->reliable = $reliable;

        return $this;
    }

    public function getGlobalgrade(): ?float
    {
        return $this->globalgrade;
    }

    public function setGlobalgrade(?float $globalgrade): self
    {
        $this->globalgrade = $globalgrade;

        return $this;
    }

    public function getIdcomment()
    {
        return $this->idcomment;
    }

    public function setIdcomment(int $idcomment): self
    {
        $this->idcomment = $idcomment;

        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getIdarticle(): Collection
    {
        return $this->idarticle;
    }

    public function addIdarticle(Article $idarticle): self
    {
        if (!$this->idarticle->contains($idarticle)) {
            $this->idarticle[] = $idarticle;
        }

        return $this;
    }

    public function removeIdarticle(Article $idarticle): self
    {
        if ($this->idarticle->contains($idarticle)) {
            $this->idarticle->removeElement($idarticle);
        }

        return $this;
    }

}
