<?php

namespace App\Entity;

//Required for PDF conversion
$parent1 = dirname(__DIR__);
$parent2 = dirname($parent1);
require $parent2.'/vendor/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\File;
use Spipu\Html2Pdf\MyPdf;

/**
 * Onlineorder
 *
 * @ORM\Table(name="onlineOrder", uniqueConstraints={@ORM\UniqueConstraint(name="idonlineOrder_UNIQUE", columns={"idonlineOrder"})})
 * @ORM\Entity(repositoryClass="App\Repository\OnlineOrderRepository")
 */
class Onlineorder
{
    public $STATUSES = array('PENDING', 'VALIDATED', 'DECLINED', 'CANCELED', 'ORDERED', 'ARRIVED', 'PROCESSED');

    /**
     * @var int
     *
     * @ORM\Column(name="idonlineOrder", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idonlineorder;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="pdfContent", type="string", nullable=false)
     */
    private $pdfContent;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="inspectionDate", type="datetime", nullable=true, options={"default"="NULL"})
     */

    private $inspectiondate = null;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=45, nullable=false)
     */
    private $status = "PENDING";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="datetime", nullable=false)
     *
     */
    private $creationdate;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUser", referencedColumnName="idUser")
     * })
     */
    private $iduser;

    /**
     * @var bool
     *
     * @ORM\Column(name="urgent", type="boolean")
     */
    private $urgent;

    /**
     * @var bool
     *
     * @ORM\Column(name="personal", type="boolean")
     */
    private $personal;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Article", mappedBy="idonlineorder")
     */
    private $idart;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idart = new ArrayCollection();
        $this->setCreationdate(new \DateTime('now'));
    }

    public function getIdonlineorder(): ?int
    {
        return $this->idonlineorder;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getInspectiondate(): ?\DateTimeInterface
    {
        return $this->inspectiondate;
    }

    public function setInspectiondate(?\DateTimeInterface $inspectiondate): self
    {
        $this->inspectiondate = $inspectiondate;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreationdate(): ?\DateTimeInterface
    {
        return $this->creationdate;
    }

    public function getCreationdateString(): ?string
    {
        return $this->creationdate->format('Y-m-d H:i:s');
    }


    public function setCreationdate(\DateTimeInterface $creationdate): self
    {
        $this->creationdate = $creationdate;

        return $this;
    }

    public function getIduser(): ?User
    {
        return $this->iduser;
    }

    public function setIduser(?User $iduser): self
    {
        $this->iduser = $iduser;

        return $this;
    }

    public function geturgent(): ?bool
    {
        return $this->urgent;
    }

    public function seturgent($urgent): self
    {
        $this->urgent = $urgent;

        return $this;
    }

    public function getPersonal(): ?bool
    {
        return $this->personal;
    }

    public function setPersonal($personal): self
    {
        $this->personal = $personal;

        return $this;
    }

    public function getPdfContent(){
        return $this->pdfContent;
    }

    public function setPdfContent(string $new): self{
        $this->pdfContent=$new;
        return $this;
    }

    /**
     * @return Collection|Article[]
     */
    public function getIdart(): Collection
    {
        return $this->idart;
    }

    public function addIdart(Article $idart): self
    {
        if (!$this->idart->contains($idart)) {
            $this->idart[] = $idart;
            $idart->addIdonlineorder($this);
        }

        return $this;
    }

    public function removeIdart(Article $idart): self
    {
        if ($this->idart->contains($idart)) {
            $this->idart->removeElement($idart);
            $idart->removeIdonlineorder($this);
        }

        return $this;
    }

}