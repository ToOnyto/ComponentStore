<?php

namespace App\Entity;

use App\Entity\Provider;
use Symfony\Component\Validator\Constraints as Assert;


class TemporaryArticle{

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $price;

    /**
     * @var string
     */
    private $providerName;

    /**
     * @var int
     * @Assert\GreaterThan(0, message="You have to put a positive number here")
     */
    private $quantity;

    public function getName(){
        return $this->name;
    }

    public function getPrice(){
        return $this->price;
    }

    public function setName($new){
        $this->name=$new;
        return $this;
    }

    public function setPrice($new){
        $this->price=$new;
        return $this;
    }

    public function getProviderName(){
        return $this->providerName;
    }

    public function setProviderName($new){
        $this->providerName=$new;
        return $this;
    }

    public function getQuantity(){
        return $this->quantity;
    }

    public function setQuantity(int $new){
        $this->quantity=$new;
        return $this;
    }
}