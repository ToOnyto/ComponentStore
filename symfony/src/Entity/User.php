<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;
use PhpParser\Node\Scalar\String_;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="email_UNIQUE", columns={"email"}), @ORM\UniqueConstraint(name="iduser_UNIQUE", columns={"idUser"})})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(
 *     fields={"email"},
 *     message="Email already taken"
 *     )
 */
class User implements UserInterface
{

    // Defines the different role of the application
    public $ROLES = array('ROLE_STUDENT', 'ROLE_TEACHER', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN');

    /**
     * @var int
     *
     * @ORM\Column(name="idUser", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $iduser;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=15, nullable=false)
     */
    private $first_name;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=15, nullable=false)
     */
    private $last_name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=45, nullable=false)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=1000, nullable=false)
     * @Assert\Length(min="8", minMessage="Too short : 8 character minimum")
     */
    private $password;

    /**
     * @var string
     *
     * @Assert\EqualTo(propertyPath="password", message="Passwords not corresponding")
     */
    public $confirm_password;

    /**
     * @var bool
     *
     * @ORM\Column(name="banned", type="boolean", nullable=false)
     */
    private $banned;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ban_date", type="datetime")
     */
    private $ban_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ban_end_date", type="datetime")
     */
    private $ban_end_date;

    /**
     * @var string
     *
     * @ORM\Column(name="securityCode", type="string")
     */
    private $security_code;

    /**
     * @var bool
     *
     * @ORM\Column(name="activated", type="boolean")
     */
    private $activated;

    /**
     * @var float|null
     *
     * @ORM\Column(name="evaluation", type="float", nullable=true, options={"default"="NULL"})
     */
    private $evaluation = null;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /// GETTERS AND SETTERS     ///

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Onlineorder", mappedBy="iduser")
     */
    private $Onlineorders;


    public function __construct()
    {
        $this->Onlineorders=new ArrayCollection();
        $this->idproviders=new ArrayCollection();
    }

    public function getIduser(): ?int
    {
        return $this->iduser;
    }

    public function getFirstname(): ?string
    {
        return $this->first_name;
    }

    public function setFirstname(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->last_name;
    }

    public function setLastname(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getBanned(): ?bool
    {
        return $this->banned;
    }

    public function setBanned(bool $banned): self
    {
        $this->banned = $banned;

        return $this;
    }

    public function getBan_Date(): ?\DateTime
    {
        return $this->ban_date;
    }

    public function getBan_Date_String(): ?string
    {
        return $this->ban_date->format("Y-m-d H:i:s");
    }

    public function setBan_date( $ban_date): self
    {
        $this->ban_date = $ban_date;

        return $this;
    }

    public function getBan_end_date(): ?\DateTime
    {
        return $this->ban_end_date;
    }

    public function getBan_End_Date_String(): ?string
    {
        return $this->ban_end_date->format("Y-m-d H:i:s");
    }

    public function setBan_End_Date($ban_end_date): self
    {
        $this->ban_end_date = $ban_end_date;

        return $this;
    }

    public function getSecurityCode(): ?string
    {
        return $this->security_code;
    }

    public function setSecurityCode($securityCode): self
    {
        $this->security_code = $securityCode;

        return $this;
    }

    public function getActivated(): ?bool
    {
        return $this->activated;
    }

    public function setActivated($activated): self
    {
        $this->activated = $activated;

        return $this;
    }

    public function getEvaluation(){
        return $this->evaluation;
    }

    public function setEvaluation($new){
        $this->evaluation=$new;
    }

    ///     UserInterface functions

    public function getRoles(): array
    {
        $roles = $this->roles;
        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getOnlineOrders()
    {
        return $this->Onlineorders;
    }

    public function addOnlineOrder(Onlineorder $onlineorder){
        if (!$this->Onlineorders->contains($onlineorder)) {
            $this->Onlineorders[] = $onlineorder;
            $onlineorder->setIduser($this);
        }
        return $this;
    }

    public function removeOnlineOrder(Onlineorder $onlineorder){
        if ($this->Onlineorders->contains($onlineorder)) {
            $this->Onlineorders->removeElement($onlineorder);
            // set the owning side to null (unless already changed)
            if ($onlineorder->getIduser() === $this) {
                $onlineorder->setIduser(null);
            }
        }
        return $this;
    }

}