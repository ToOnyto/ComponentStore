<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grade
 *
 * @ORM\Table(name="evaluation", uniqueConstraints={@ORM\UniqueConstraint(name="idUser_UNIQUE", columns={"idRater"}), @ORM\UniqueConstraint(name="idUser_UNIQUE", columns={"idRated"}), @ORM\UniqueConstraint(name="idEvaluation_UNIQUE", columns={"idEvaluation"})})
 * @ORM\Entity
 */
class Evaluation
{
    /**
     * @var int
     *
     * @ORM\Column(name="idEvaluation", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idevaluation;

    /**
     * @var int
     *
     * @ORM\Column(name="rate", type="integer", nullable=false)
     */
    private $rate;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idrater", referencedColumnName="idUser")
     * })
     */
    private $idrater;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idrated", referencedColumnName="idUser")
     * })
     */
    private $idrated;

    public function getIdevaluation()
    {
        return $this->idevaluation;
    }

    public function getRate()
    {
        return $this->rate;
    }

    public function setRate(int $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getIdrater()
    {
        return $this->idrater;
    }

    public function setIdrater(?User $idrater): self
    {
        $this->idrater = $idrater;

        return $this;
    }

    public function getIdrated()
    {
        return $this->idrated;
    }

    public function setIdrated(?User $idrated): self
    {
        $this->idrated = $idrated;

        return $this;
    }


}
