<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Article
 *
 * @ORM\Table(name="article", uniqueConstraints={@ORM\UniqueConstraint(name="articleName_UNIQUE", columns={"articleName"}), @ORM\UniqueConstraint(name="idArticle_UNIQUE", columns={"idArticle"})})
 * @ORM\Entity
 */
class Article
{
    /**
     * @var int
     *
     * @ORM\Column(name="idArticle", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idarticle;

    /**
     * @var string
     *
     * @ORM\Column(name="articleName", type="string", length=30, nullable=false)
     */
    private $articlename;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Onlineorder", inversedBy="idart")
     * @ORM\JoinTable(name="contains",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idArt", referencedColumnName="idArticle")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="idonlineOrder", referencedColumnName="idonlineOrder")
     *   }
     * )
     */
    private $idonlineorder;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Provider", mappedBy="idarticle")
     */
    private $idprovider;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idonlineorder = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idprovider = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdarticle(): ?int
    {
        return $this->idarticle;
    }

    public function getArticlename(): ?string
    {
        return $this->articlename;
    }

    public function setArticlename(string $articlename): self
    {
        $this->articlename = $articlename;

        return $this;
    }

    /**
     * @return Collection|Onlineorder[]
     */
    public function getIdonlineorder(): Collection
    {
        return $this->idonlineorder;
    }

    public function addIdonlineorder(Onlineorder $idonlineorder): self
    {
        if (!$this->idonlineorder->contains($idonlineorder)) {
            $this->idonlineorder[] = $idonlineorder;
        }

        return $this;
    }

    public function removeIdonlineorder(Onlineorder $idonlineorder): self
    {
        if ($this->idonlineorder->contains($idonlineorder)) {
            $this->idonlineorder->removeElement($idonlineorder);
        }

        return $this;
    }

    /**
     * @return Collection|Provider[]
     */
    public function getIdprovider(): Collection
    {
        return $this->idprovider;
    }

    public function addIdprovider(Provider $idprovider): self
    {
        if (!$this->idprovider->contains($idprovider)) {
            $this->idprovider[] = $idprovider;
            $idprovider->addIdarticle($this);
        }

        return $this;
    }

    public function removeIdprovider(Provider $idprovider): self
    {
        if ($this->idprovider->contains($idprovider)) {
            $this->idprovider->removeElement($idprovider);
            $idprovider->removeIdarticle($this);
        }

        return $this;
    }

}
