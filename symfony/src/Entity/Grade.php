<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grade
 *
 * @ORM\Table(name="grade", uniqueConstraints={@ORM\UniqueConstraint(name="idProvider_UNIQUE", columns={"idProvider"}), @ORM\UniqueConstraint(name="idUser_UNIQUE", columns={"idUser"}), @ORM\UniqueConstraint(name="idGrade_UNIQUE", columns={"idGrade"})})
 * @ORM\Entity
 */
class Grade
{
    /**
     * @var int
     *
     * @ORM\Column(name="idGrade", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idgrade;

    /**
     * @var int
     *
     * @ORM\Column(name="rate", type="integer", nullable=false)
     */
    private $rate;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idUser", referencedColumnName="idUser")
     * })
     */
    private $iduser;

    /**
     * @var \Provider
     *
     * @ORM\ManyToOne(targetEntity="Provider")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idProvider", referencedColumnName="idProvider")
     * })
     */
    private $idprovider;

    public function getIdgrade(): ?int
    {
        return $this->idgrade;
    }

    public function getRate(): ?int
    {
        return $this->rate;
    }

    public function setRate(int $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getIduser(): ?User
    {
        return $this->iduser;
    }

    public function setIduser(?User $iduser): self
    {
        $this->iduser = $iduser;

        return $this;
    }

    public function getIdprovider(): ?Provider
    {
        return $this->idprovider;
    }

    public function setIdprovider(?Provider $idprovider): self
    {
        $this->idprovider = $idprovider;

        return $this;
    }


}
