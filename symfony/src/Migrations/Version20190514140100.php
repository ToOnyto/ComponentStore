<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190514140100 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE role');
        $this->addSql('ALTER TABLE provider CHANGE idComment idComment INT DEFAULT NULL');
        $this->addSql('ALTER TABLE provides DROP INDEX idArticle_UNIQUE, ADD INDEX IDX_E5C3430A12836594 (idArticle)');
        $this->addSql('ALTER TABLE provides DROP INDEX idProvider_UNIQUE, ADD INDEX IDX_E5C3430AC16759C7 (idProvider)');
        $this->addSql('ALTER TABLE user CHANGE password password VARCHAR(1000) NOT NULL, CHANGE roles roles JSON NOT NULL, CHANGE ban_date ban_date DATETIME NOT NULL, CHANGE ban_end_date ban_end_date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE grade CHANGE idUser idUser INT DEFAULT NULL, CHANGE idProvider idProvider INT DEFAULT NULL');
        $this->addSql('ALTER TABLE onlineOrder CHANGE idUser idUser INT DEFAULT NULL');
        $this->addSql('ALTER TABLE contains DROP INDEX idOrder_UNIQUE, ADD INDEX IDX_8EFA6A7EA5CDBC8E (idonlineOrder)');
        $this->addSql('ALTER TABLE contains DROP INDEX idArt_UNIQUE, ADD INDEX IDX_8EFA6A7EDD7DCBD3 (idArt)');
        $this->addSql('ALTER TABLE comment CHANGE idUser idUser INT DEFAULT NULL, CHANGE idProvider idProvider INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `order` (idOrder INT AUTO_INCREMENT NOT NULL, description VARCHAR(200) NOT NULL COLLATE utf8_general_ci, inspectionDate DATETIME NOT NULL, status INT NOT NULL, creationDate DATETIME NOT NULL, idUser INT NOT NULL, UNIQUE INDEX idOrder_UNIQUE (idOrder), INDEX fk_order_1_idx (idUser), UNIQUE INDEX idUser_UNIQUE (idUser), PRIMARY KEY(idOrder)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE role (idRole INT AUTO_INCREMENT NOT NULL, roleName VARCHAR(15) NOT NULL COLLATE utf8_general_ci, UNIQUE INDEX idRole_UNIQUE (idRole), UNIQUE INDEX roleName_UNIQUE (roleName), PRIMARY KEY(idRole)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT fk_order_1 FOREIGN KEY (idUser) REFERENCES user (idUser) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE comment CHANGE idUser idUser INT NOT NULL, CHANGE idProvider idProvider INT NOT NULL');
        $this->addSql('ALTER TABLE contains DROP INDEX IDX_8EFA6A7EDD7DCBD3, ADD UNIQUE INDEX idArt_UNIQUE (idArt)');
        $this->addSql('ALTER TABLE contains DROP INDEX IDX_8EFA6A7EA5CDBC8E, ADD UNIQUE INDEX idOrder_UNIQUE (idonlineOrder)');
        $this->addSql('ALTER TABLE grade CHANGE idUser idUser INT NOT NULL, CHANGE idProvider idProvider INT NOT NULL');
        $this->addSql('ALTER TABLE onlineOrder CHANGE idUser idUser INT NOT NULL');
        $this->addSql('ALTER TABLE provider CHANGE idComment idComment INT NOT NULL');
        $this->addSql('ALTER TABLE provides DROP INDEX IDX_E5C3430AC16759C7, ADD UNIQUE INDEX idProvider_UNIQUE (idProvider)');
        $this->addSql('ALTER TABLE provides DROP INDEX IDX_E5C3430A12836594, ADD UNIQUE INDEX idArticle_UNIQUE (idArticle)');
        $this->addSql('ALTER TABLE user CHANGE password password VARCHAR(300) NOT NULL COLLATE utf8_general_ci, CHANGE ban_date ban_date DATETIME DEFAULT \'NULL\', CHANGE ban_end_date ban_end_date VARCHAR(45) DEFAULT \'NULL\' COLLATE utf8_general_ci, CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
