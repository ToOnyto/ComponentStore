<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IndexControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

    }

//    public function testSignInButton(){
//
//        $client = static::createClient();
//
//        $crawler = $client->request('GET', '/');
//
//        $form = $crawler->selectButton('sign in')->form();
//
//        $client->submit($form);
//
//        $this->assertTrue($client->getResponse()->isRedirect('/user_registration'));
//
//    }
}