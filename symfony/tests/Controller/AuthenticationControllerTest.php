<?php

namespace App\Tests\Controller;

use App\Controller\AuthenticationController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthenticationControllerTest extends WebTestCase {


    public function testRegistration(){

        $client = static::createClient();

        $crawler = $client->request('GET', '/registration');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $buttonCrawlerNode = $crawler->selectButton('Sign Up');

// you can also pass an array of field values that overrides the default ones
        $form = $buttonCrawlerNode->form([
            'signupForm[first_name]' => 'Fabien',
            'signupForm[last_name]' => 'Fabien',
            'signupForm[email]' => "tony.pedrero@etu.u-bordeaux.fr",
            'signupForm[password]' => 'azertyui',
            'signupForm[confirm_password]' => 'azertyui',
        ]);

// submit the Form object
        $client->submit($form);


        $this->assertTrue($client->getResponse()->isSuccessful());
    }
}